import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handlePasswordChange,
  handlePasswordConfirmationChange,
  handleUsernameChange,
  error,
  reset,
} from "./features/auth/signupSlice";
import ErrorMessage from "./ErrorMessage";
import { useSignupMutation } from "./services/goals";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useRef } from "react";
import { useNavigate } from "react-router-dom";

const Signup = () => {
  const dispatch = useDispatch();
  const { errorMessage, fields } = useSelector((state) => state.signup);
  const [callSignUp] = useSignupMutation();
  const [open, setOpen] = useState(true);
  const cancelButtonRef = useRef(null);

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [cmfPassword, setCmfPassword] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    // Password does not match
    if (cmfPassword !== password) {
      dispatch(error("Password does not match confirmation"));
    } else {
      // password matches
      callSignUp({ username, password });
      dispatch(reset());
      navigate("/goals/list");
    }
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative overflow-hidden text-left transition-all transform rounded-lg shadow-xl sm:my-8 sm:w-full sm:max-w-lg">
                <div className="px-4 pt-5 pb-4 bg-gradient-to-br from-pink-300 to-blue-400 sm:p-6 sm:pb-4 rounded-t-lg">
                  <div className="sm:flex sm:items-start">
                    <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                      <Dialog.Title
                        as="h3"
                        className="text-base font-semibold leading-6 text-gray-900"
                      ></Dialog.Title>
                      <div className="card mx-auto items-center">
                        <div className="card-body">
                          <p class="text-base/7 ...">
                            <h2 className="text-base font-semibold leading-7 text-gray-900">
                              Sign up for HealthFul Hunt!
                            </h2>

                            <hr />
                          </p>
                          <br />

                          <form onSubmit={handleSubmit}>
                            {errorMessage && (
                              <ErrorMessage>{errorMessage}</ErrorMessage>
                            )}
                            <div className="col-span-full">
                              <label
                                htmlFor="Signup__username"
                                className="form-label"
                              >
                                Username:
                              </label>
                              <input
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                type={`text`}
                                id="Signup__username"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                placeholder="Enter a username"
                                style={{ fontStyle: "italic" }}
                              />
                            </div>
                            <div className="mb-3">
                              <label
                                htmlFor="Signup__password"
                                className="form-label"
                              >
                                Password:
                              </label>
                              <input
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                type={`password`}
                                id="Signup__password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                placeholder="Enter a password"
                                style={{ fontStyle: "italic" }}
                              />
                            </div>
                            <div className="mb-3">
                              <label
                                htmlFor="Signup__password_confirmation"
                                className="form-label"
                              >
                                Confirm Password:
                              </label>
                              <input
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                type={`password`}
                                id="Signup__password_confirmation"
                                value={cmfPassword}
                                onChange={(e) => setCmfPassword(e.target.value)}
                                placeholder="Confirm the password"
                                style={{ fontStyle: "italic" }}
                              />
                            </div>
                            <button
                              type="submit"
                              style={{
                                backgroundColor: "green",
                                color: "white",
                                padding: "10px 20px",
                                borderRadius: "5px",
                                border: "none",
                                fontSize: "1.1em",
                              }}
                              className="btn btn-success"
                            >
                              Sign up
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default Signup;
