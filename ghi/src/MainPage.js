import React from "react";
import "./mainPage.css";

function MainPage() {
  return (
    <div className="text-center">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="text-3xl font-bold underline">HelpfulHunt</h1>
        <div className="mx-auto col-lg-6">
          <p className="mb-4 lead">
            The premiere solution for building healthy habits!
          </p>
        </div>
      </div>
      <div
        className="background-image"
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: "90vh",
          border: "3px solid black",
          padding: "10px",
          width: "80vw",
          borderRadius: "20px",
          overflowX: "auto",
          marginBottom: "20px",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "100%",
            height: "10%",
            marginTop: "20px",
          }}
        ></div>
        <div></div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "10px",
            width: "120%",
            height: "40%",
            borderRadius: "20px",
            marginBottom: "20px",
            marginTop: "-70vh",
          }}
        >
          <a href="/signup">
            <button
              style={{
                width: "160px",
                height: "50px",
                margin: "0 10px",
                border: "none",
                backgroundColor: "red",
                color: "white",
                borderRadius: "10px",
                fontSize: "18px",
                fontWeight: "bold",
              }}
            >
              GET STARTED
            </button>
          </a>
        </div>
      </div>
      <div className="border-2 border-black p-25 rounded-lg ">
        <div className="flex justify-center">
          <div className="mx-auto flex flex-col items-center justify-center">
            <div className="border-2 border-black w-2/4 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
              <p className="text-center px-4 font-bold">HOW IT WORKS</p>
            </div>
            <div className="border-2 border-black w-4/4 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
              <p className="text-center px-4 font-bold italic">
                BEHAVIOURISTS HAVE IDENTIFIED 3 SIMPLE STEPS TO ACHIEVING ANY
                GOAL:
              </p>
            </div>
          </div>
        </div>
        <div className="flex justify-center w-full">
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic">1. SET A GOAL</p>
          </div>
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic">
              2. ACTUALLY DO IT
            </p>
          </div>
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic ">
              3. TRACK YOUR PROGRESS
            </p>
          </div>
        </div>
        <div className="flex justify-between w-full">
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic">
              Select what habits you would like to develop or quit and start
              your journey
            </p>
          </div>
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic">
              We help keep you accountable by sending you reminders at intervals
              that you decide, to ensure that you never forget!
            </p>
          </div>
          <div className="border-2 border-black w-full md:w-1/3 xl:w-1/7 h-64 md:h-auto bg-gray-200 m-2 rounded-lg flex justify-center items-center">
            <p className="text-center px-4 font-bold italic">
              See your daily, weekly or monthly progress as you work towards
              meeting your goals
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
