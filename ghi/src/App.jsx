import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Login from "./Login";
import Signup from "./Signup";
import Logout from "./Logout";
import NewGoal from "./newGoal";
import Goals from "./ListGoals";
import UpdateGoal from "./updateGoal";
import { useGetAccountQuery } from "./services/goals";
import Nav from "./Nav";
import { ModalComp } from "./components/ModalComp";
import { useSelector } from "react-redux";
import { useEffect, useRef } from "react";
import autoAnimate from "@formkit/auto-animate";

function App() {
  const { data: account } = useGetAccountQuery();
  console.log({ account });
  const modalComp = useSelector((state) => state.modalSlice);

  const parent = useRef(null);

  useEffect(() => {
    parent.current && autoAnimate(parent.current);
  }, [parent]);

  return (
    <div>
      <BrowserRouter>
        <Nav />

        <div ref={parent}>{modalComp.isActive && <ModalComp />}</div>

        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/signup" element={<Signup />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="login" element={<Login />} />

            <Route path="goals">
              <Route path="new" element={<NewGoal />} />
              <Route path="list" element={<Goals />} />
              <Route path=":id" element={<UpdateGoal />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
