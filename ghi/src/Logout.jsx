import { useLogoutMutation } from "./services/goals";
import { useNavigate } from "react-router-dom";

const Logout = () => {
  const [logout] = useLogoutMutation();
  const navigate = useNavigate();

  const handleLogout = async () => {
    await logout().then((resp) => {
      console.log("Logging Out");
      if (resp.data) {
        console.log("Nav to home");
        navigate("/");
      }
    });
  };

  return (
    <button
      className="btn btn-danger"
      onClick={() => console.log("Logout Button clicked")}
    >
      Logout
    </button>
  );
};

export default Logout;
