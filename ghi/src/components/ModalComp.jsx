import { useDispatch, useSelector } from "react-redux";
import { toggleModal } from "../features/auth/modalSlice";
import { useState } from "react";
import { useLoginMutation } from "../services/goals";
import { useNavigate } from "react-router-dom";

export const ModalComp = () => {
  const { isActive } = useSelector((state) => state.modalSlice);
  const [callLogin, callLoginResponse] = useLoginMutation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const hideLoginModal = () => {
    if (isActive) {
      dispatch(toggleModal());
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    callLogin({
      username,
      password,
    }).then((abc) => {
      if (abc.data) {
        setPassword("");
        setUsername("");
        hideLoginModal();
        navigate("/goals/list");
      }
    });
  };

  return (
    <div
      className="fixed z-10 inset-0 overflow-y-auto"
      style={{ backgroundColor: "rgba(0, 0, 0, 0.4)" }}
    >
      <div
        className="fixed flex flex-col justify-center items-center w-screen h-screen min-h-full p-4 bg-aqua-300 lg:p-8 shadow-xl"
        style={{
          position: "fixed",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",

        }}
      >
        <div className="sm:mx-auto sm:w-full sm:max-w-sm">
          <h2 className="mt-10 text-2xl font-bold italic leading-9 tracking-tight text-center text-gray-900">
            Log in to your account
          </h2>
        </div>

        <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
          <form className="space-y-6" onSubmit={handleSubmit}>
            <div>
              <label
                htmlFor="email"
                className="block text-lg font-medium leading-6 text-gray-900"
              >
                Username
              </label>
              <div className="mt-2">
                <input
                  id="email"
                  name="email"
                  type="text"
                  required
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  className="block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm bg-gray-100"
                  placeholder="Enter your username"
                  style={{ fontStyle: "italic" }}
                />
              </div>
            </div>

            <div>
              <div className="flex items-center justify-between">
                <label
                  htmlFor="password"
                  className="block text-lg font-medium leading-6 text-gray-900"
                >
                  Password
                </label>
              </div>
              <div className="mt-2">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  className="block w-full py-2 px-3 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500 sm:text-sm bg-gray-100"
                  placeholder="Enter your password"
                  style={{ fontStyle: "italic" }}
                />
              </div>
            </div>

            <div>
              <button
                type="submit"
                className="flex w-full justify-center rounded-md bg-gray-600 px-3 py-1.5 text-lg font-semibold leading-6 text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Log in
              </button>
              <div className="py-3"></div>
              <button
                type="button"
                onClick={() => hideLoginModal()}
                className=" flex w-full justify-center rounded-md bg-gray-600 px-3 py-1.5 text-lg font-semibold leading-6 text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
              >
                Cancel
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
