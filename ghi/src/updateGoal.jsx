import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { useGetGoalByIdQuery } from "./services/goals";
import {
  handleNameChange,
  handleFrequencyChange,
  handleIconUrlChange,
  handleStartDateChange,
  handleAllFieldsChange,
  reset,
} from "./features/goal/updateGoalSlice";
import { selectGoal } from "..//src/features/goal/selectGoalSlice";
import { useUpdateGoalMutation } from "./services/goals";
import { useNavigate } from "react-router-dom";

function UpdateGoal() {
  const dispatch = useDispatch();
  const { id } = useParams();
  const { data: goalData, isLoading } = useGetGoalByIdQuery(id);
  const formFields = useSelector((state) => state.updateGoal);
  const navigate = useNavigate();
  const [updateGoalMutation] = useUpdateGoalMutation();
  const handleFormSubmit = (e) => {
    e.preventDefault();
    updateGoalMutation({
      id: id,

      name: formFields.name,
      frequency: formFields.frequency,
      icon_url: formFields.icon_url,
      start_date: formFields.start_date,
    }).then((resp) => {
      if (resp.data) {
        // console.log("reroute");
        navigate("/goals/list");
      }
    });
  };
  React.useEffect(() => {
    if (goalData) {
      dispatch(selectGoal(goalData)); // Store the selected goal in the Redux store
      dispatch(handleNameChange(goalData.name));
      dispatch(handleFrequencyChange(goalData.frequency));
      dispatch(handleIconUrlChange(goalData.icon_url));
      dispatch(handleStartDateChange(goalData.start_date));
    }
  }, [dispatch, goalData]);

  return (
    <form onSubmit={handleFormSubmit}>
      <div className="bg-white rounded shadow-md p-3 mt-5">
        <label
          htmlFor="name-field"
          className="form-label text-gray-700 font-bold"
        >
          Name
        </label>
        <input
          type="text"
          className="form-control border border-gray-300 outline-none focus:ring focus:ring-blue-500"
          id="name-field"
          placeholder="Goal 1"
          tabIndex={1}
          value={formFields.name}
          onChange={(e) => dispatch(handleNameChange(e.target.value))}
        />
      </div>
      <div className="bg-white rounded shadow-md p-3 mt-5">
        <label
          htmlFor="frequency-field"
          className="form-label text-gray-700 font-bold"
        >
          Frequency
        </label>
        <input
          type="int"
          className="form-control"
          id="frequency-field"
          placeholder="frequency"
          tabIndex={1}
          value={formFields.frequency}
          onChange={(e) => dispatch(handleFrequencyChange(e.target.value))}
        />
      </div>
      <div className="bg-white rounded shadow-md p-3 mt-5">
        <label htmlFor="Icon_Url-field" className="form-label">
          Icon url
        </label>
        <input
          type="text"
          className="form-control"
          id="icon_url-field"
          placeholder="Icon Url"
          tabIndex={1}
          value={formFields.icon_url}
          onChange={(e) => dispatch(handleIconUrlChange(e.target.value))}
        />
      </div>
      <div className="bg-white rounded shadow-md p-3 mt-5">
        <label htmlFor="start_date-field" className="form-label">
          Start Date
        </label>
        <input
          type="date"
          className="form-control"
          id="date-field"
          placeholder="YYYY-MM-DD"
          tabIndex={1}
          value={formFields.start_date}
          onChange={(e) => dispatch(handleStartDateChange(e.target.value))}
        />
      </div>
      <button className="bg-violet-500 hover:bg-violet-600 active:bg-violet-700 focus:outline-none focus:ring focus:ring-violet-300 ...">
        Submit
      </button>
    </form>
  );
}
export default UpdateGoal;
