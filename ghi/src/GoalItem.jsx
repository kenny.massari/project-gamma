import React from "react";
import { useDeleteGoalMutation } from "./services/goals";
import { Link } from "react-router-dom";
import {
  useCreateActionsMutation,
  useGetActionsQuery,
  useDeleteActionsMutation,
} from "./services/goals";

const GoalItem = ({ id, name, frequency }) => {
  const [deleteGoal] = useDeleteGoalMutation();
  const [createAction] = useCreateActionsMutation();
  const [deleteAction] = useDeleteActionsMutation();

  const handleCreateAction = () => {
    const dateCompleted = new Date().toISOString().slice(0, 10);
    createAction({ goal_id: id, dateCompleted: dateCompleted });
  };

  const dateCompleted = new Date().toISOString().slice(0, 10);
  const { data } = useGetActionsQuery({
    goal_id: id,
    date_completed: dateCompleted,
  });
  console.log("data:", data);

  const count = data?.length;

  const handleDeleteAction = async () => {
    if (data.length > 0) {
      try {
        console.log("Deleted action");
        await deleteAction(data[0].id);
      } catch (error) {
        console.log("failed to delete");
      }
    }
  };

  return (
    <li className="flex justify-between p-3 my-3 bg-blue-200">
      {/* {name}
      {` `}
      <div>{count}</div>
      <Link className="btn btn-sm btn-primary" to={`/goals/${id}`}>
        Update
      </Link>

      <button className="btn btn-sm btn-danger" onClick={handleCreateAction}>
        +
      </button> */}
      <div className="flex gap-x-3">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="self-center w-10 h-10 border-2 rounded-full"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M15.362 5.214A8.252 8.252 0 0112 21 8.25 8.25 0 016.038 7.048 8.287 8.287 0 009 9.6a8.983 8.983 0 013.361-6.867 8.21 8.21 0 003 2.48z"
          />
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M12 18a3.75 3.75 0 00.495-7.467 5.99 5.99 0 00-1.925 3.546 5.974 5.974 0 01-2.133-1A3.75 3.75 0 0012 18z"
          />
        </svg>

        <div>
          <div className="self-center">{name}</div>
          <div className="self-center">
            {count >= frequency ? "Completed" : `${count}/${frequency}`}
          </div>
        </div>
      </div>
      <div className="flex">
        <button className="btn btn-sm btn-danger" onClick={handleCreateAction}>
          {/* <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="self-center w-10 h-10"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 4.5v15m7.5-7.5h-15"
            />
          </svg> */}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            fill="blue"
            class="bi bi-cloud-plus-fill blue-500"
            viewBox="0 0 16 16"
          >
            <path d="M8 2a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13h8.906C14.502 13 16 11.57 16 9.773c0-1.636-1.242-2.969-2.834-3.194C12.923 3.999 10.69 2 8 2zm.5 4v1.5H10a.5.5 0 0 1 0 1H8.5V10a.5.5 0 0 1-1 0V8.5H6a.5.5 0 0 1 0-1h1.5V6a.5.5 0 0 1 1 0z" />
          </svg>
        </button>
        <button className="btn btn-sm btn-danger" onClick={handleDeleteAction}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            fill="red"
            class="bi bi-cloud-minus-fill"
            viewBox="0 0 16 16"
          >
            <path d="M8 2a5.53 5.53 0 0 0-3.594 1.342c-.766.66-1.321 1.52-1.464 2.383C1.266 6.095 0 7.555 0 9.318 0 11.366 1.708 13 3.781 13h8.906C14.502 13 16 11.57 16 9.773c0-1.636-1.242-2.969-2.834-3.194C12.923 3.999 10.69 2 8 2zM6 7.5h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1 0-1z" />
          </svg>
        </button>
        <Link className="btn btn-sm btn-primary" to={`/goals/${id}`}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="self-center w-10 h-10"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M12 6.75a.75.75 0 110-1.5.75.75 0 010 1.5zM12 12.75a.75.75 0 110-1.5.75.75 0 010 1.5zM12 18.75a.75.75 0 110-1.5.75.75 0 010 1.5z"
            />
          </svg>
        </Link>
        <button
          className="btn btn-sm btn-danger"
          onClick={(e) => deleteGoal(id)}
        >
          Delete
        </button>
      </div>
    </li>
  );
};

export default GoalItem;
