import React from "react";
import { useGetGoalsQuery } from "./services/goals";
import GoalItem from "./GoalItem";

const Goals = () => {
  const { data, isLoading } = useGetGoalsQuery();
  if (isLoading) return <div>Loading...</div>;
  if (!data || data.length === 0) return <div>No goals</div>;

  return (
    <ul>
      {data.map((goal) => (
        <GoalItem key={goal.id} {...goal} />
      ))}
    </ul>
  );
};

export default Goals;
