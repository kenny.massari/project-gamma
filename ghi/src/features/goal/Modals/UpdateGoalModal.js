import React, { useState } from "react";

const GoalModal = ({ goal, onClose, onSave }) => {
  const [name, setName] = useState(goal.name);
  const [frequency, setFrequency] = useState(goal.frequency);
  const [iconUrl, setIconUrl] = useState(goal.icon_url);
  const [startDate, setStartDate] = useState(goal.start_date);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleFrequencyChange = (event) => {
    setFrequency(event.target.value);
  };

  const handleIconUrlChange = (event) => {
    setIconUrl(event.target.value);
  };

  const handleStartDateChange = (event) => {
    setStartDate(event.target.value);
  };

  const handleSave = () => {
    onSave({
      id: goal.id,
      name,
      frequency,
      icon_url: iconUrl,
      start_date: startDate,
    });
  };

  return (
    <div>
      <h2>Edit Goal</h2>
      <label>
        Name:
        <input type="text" value={name} onChange={handleNameChange} />
      </label>
      <label>
        Frequency:
        <input type="text" value={frequency} onChange={handleFrequencyChange} />
      </label>
      <label>
        Icon URL:
        <input type="text" value={iconUrl} onChange={handleIconUrlChange} />
      </label>
      <label>
        Start Date:
        <input type="text" value={startDate} onChange={handleStartDateChange} />
      </label>
      <button onClick={onClose}>Cancel</button>
      <button onClick={handleSave}>Save</button>
    </div>
  );
};

export default GoalModal;
