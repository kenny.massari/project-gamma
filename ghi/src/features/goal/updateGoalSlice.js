// updateGoalSlice.js

import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: "",
  frequency: "",
  icon_url: "",
  start_date: "",
};

export const updateGoalSlice = createSlice({
  name: "UpdateGoal",
  initialState,
  reducers: {
    handleNameChange: (state, action) => {
      state.name = action.payload;
    },
    handleFrequencyChange: (state, action) => {
      state.frequency = action.payload;
    },
    handleIconUrlChange: (state, action) => {
      state.icon_url = action.payload;
    },
    handleStartDateChange: (state, action) => {
      state.start_date = action.payload;
    },
    handleAllFieldsChange: (state, action) => {
      Object.assign(state, action.payload);
    },
    reset: () => initialState,
  },
});

export const {
  handleNameChange,
  handleFrequencyChange,
  handleIconUrlChange,
  handleStartDateChange,
  handleAllFieldsChange,
  reset,
} = updateGoalSlice.actions;

export default updateGoalSlice.reducer;
