import { createSlice } from "@reduxjs/toolkit";

const initialState = null;

export const selectedGoalSlice = createSlice({
  name: "selectedGoal",
  initialState,
  reducers: {
    selectGoal: (state, action) => {
      return action.payload;
    },
    reset: () => initialState,
  },
});

export const { selectGoal, reset } = selectedGoalSlice.actions;

export default selectedGoalSlice.reducer;
