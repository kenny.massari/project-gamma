import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: "",
  frequency: "",
  icon_url: "",
  start_date: "",
};

export const newGoalSlice = createSlice({
  name: "newGoal",
  initialState,
  reducers: {
    handleNameChange: (state, action) => {
      state.name = action.payload;
    },
    handleFrequencyChange: (state, action) => {
      state.frequency = action.payload;
    },
    handleIconUrlChange: (state, action) => {
      state.icon_url = action.payload;
    },
    handleStartDateChange: (state, action) => {
      state.start_date = action.payload;
    },
    handleAllFieldsChange: (state, action) => {
      const { name, frequency, icon_url, start_date } = action.payload;
      state.name = name;
      state.frequency = frequency;
      state.icon_url = icon_url;
      state.start_date = start_date;
    },
    reset: () => initialState,
  },
});

export const {
  handleNameChange,
  handleFrequencyChange,
  handleIconUrlChange,
  handleStartDateChange,
  handleAllFieldsChange,
  reset,
} = newGoalSlice.actions;

export default newGoalSlice.reducer;
