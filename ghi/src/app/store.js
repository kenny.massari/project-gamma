import { configureStore } from "@reduxjs/toolkit";
import newGoalReducer from "../features/goal/newGoalSlice";
import loginReducer from "../features/auth/loginSlice";
import signupReducer from "../features/auth/signupSlice";
import { setupListeners } from "@reduxjs/toolkit/query";
import { actionsApi, goalsApi } from "../services/goals";
import { authApi } from "../services/goals";
import updateGoalReducer from "../features/goal/updateGoalSlice";
import modalReducer from "../features/auth/modalSlice";

export const store = configureStore({
  reducer: {
    newGoal: newGoalReducer,
    login: loginReducer,
    signup: signupReducer,
    [goalsApi.reducerPath]: goalsApi.reducer,
    [authApi.reducerPath]: authApi.reducer,
    updateGoal: updateGoalReducer,
    [actionsApi.reducerPath]: actionsApi.reducer,
    modalSlice: modalReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      goalsApi.middleware,
      authApi.middleware,
      actionsApi.middleware,
    ]),
});

setupListeners(store.dispatch);
