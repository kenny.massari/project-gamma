import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handlePasswordChange,
  handleUsernameChange,
  reset,
} from "./features/auth/loginSlice";
import { useLoginMutation } from "./services/goals";
import ErrorMessage from "./ErrorMessage";
import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useRef, useState } from "react";

const Login = () => {
  const dispatch = useDispatch();
  const [login] = useLoginMutation();
  const { fields } = useSelector((state) => state.login);
  const [open, setOpen] = useState(true);
  const cancelButtonRef = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("handleSubmit");
    console.log({ fields });
    login(fields);
    dispatch(reset());
  };

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog
        as="div"
        className="relative z-10"
        initialFocus={cancelButtonRef}
        onClose={setOpen}
      >
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-75" />
        </Transition.Child>

        <div className="fixed inset-0 z-10 overflow-y-auto">
          <div className="flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="relative overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl sm:my-8 sm:w-full sm:max-w-lg">
                <div className="px-4 pt-5 pb-4 bg-white sm:p-6 sm:pb-4">
                  <div className="sm:flex sm:items-start">
                    <div className="mt-3 text-center sm:ml-4 sm:mt-0 sm:text-left">
                      <Dialog.Title
                        as="h3"
                        className="text-base font-semibold leading-6 text-gray-900"
                      >
                        Login
                      </Dialog.Title>
                      <div className="card">
                        <div className="card-body">
                          <ErrorMessage></ErrorMessage>
                          <hr />
                          <form onSubmit={handleSubmit}>
                            <div className="mb-3">
                              <label
                                htmlFor="Login__username"
                                className="form-label"
                              >
                                Username:
                              </label>
                              <input
                                className="form-control form-control-sm"
                                type={`text`}
                                id="Login__username"
                                value={fields.username}
                                onChange={(e) =>
                                  dispatch(handleUsernameChange(e.target.value))
                                }
                              />
                            </div>
                            <div className="mb-3">
                              <label
                                htmlFor="Login__password"
                                className="form-label"
                              >
                                Password:
                              </label>
                              <input
                                className="form-control form-control-sm"
                                type={`password`}
                                id="Login__password"
                                value={fields.password}
                                onChange={(e) =>
                                  dispatch(handlePasswordChange(e.target.value))
                                }
                              />
                            </div>
                            <button
                              type="submit"
                              className="inline-flex justify-center w-full px-3 py-2 text-sm font-semibold text-white bg-red-600 rounded-md shadow-sm hover:bg-red-500 sm:ml-3 sm:w-auto"
                            >
                              Login
                            </button>
                            <button
                              type="button"
                              className="inline-flex justify-center w-full px-3 py-2 mt-3 text-base font-semibold text-gray-900 bg-white rounded-md shadow-sm ring-1 ring-inset ring-red-300 hover:bg-red sm:mt-0 sm:w-auto"
                              onClick={() => setOpen(false)}
                              ref={cancelButtonRef}
                            >
                              Cancel
                            </button>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default Login;
