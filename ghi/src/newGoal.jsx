import React, { useState } from "react";
import { useCreateGoalMutation } from "./services/goals";
import { ButtonComp } from "./components/ButtonComp";
import { FormInputText } from "./components/FormInputText";
import { useNavigate } from "react-router-dom";

const NewGoal = () => {
  const [name, setName] = useState("");
  const [frequency, setFrequency] = useState("");
  const [icon_url, setIcon_url] = useState("");
  const [start_date, setStartDate] = useState(new Date());
  const [createGoal] = useCreateGoalMutation();
  const navigate = useNavigate();

  return (
    <form
      className="py-3"
      onSubmit={(e) => {
        e.preventDefault();
        createGoal({ name, frequency, icon_url, start_date }).then((resp) => {
          if (resp.data) {
            navigate("/goals/list");
          }
        });
      }}
    >
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setName(e.target.value);
          }}
          value={name}
          label={"Name"}
          type={"text"}
        />
      </div>
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setFrequency(e.target.value);
          }}
          value={frequency}
          label={"Frequency"}
          type={"text"}
        />
      </div>
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setIcon_url(e.target.value);
          }}
          value={icon_url}
          label={"Icon Url"}
          type={"text"}
        />
      </div>
      <div className="mb-3">
        <FormInputText
          onChange={(e) => {
            setStartDate(e.target.value);
          }}
          value={start_date}
          label={"Start Date"}
          type={"date"}
        />
      </div>

      <ButtonComp title="Submit" type="submit" />
      <ButtonComp
        title="Reset"
        type="reset"
        className="btn btn-info"
        onClickFunc={(e) => {
          e.preventDefault();
        }}
      />
    </form>
  );
};

export default NewGoal;
