from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token
from typing import Optional
from datetime import date


# from datetime import date

class AccountOut(BaseModel): # what the front end will need
    id: str
    username: str


class AccountOutWithHashedPassword(AccountOut): # internal (FastAPI) use only
    hashed_password: str # encrypted password


class AccountIn(BaseModel):
    username: str
    password: str # user's password


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class GoalParams(BaseModel):
    name: str
    frequency: str
    icon_url: str
    start_date: str

class Goal(GoalParams):
    id: str
    user_id: str

class GoalId(BaseModel):
    id: str

    #think about this one folks

class GoalsList(BaseModel):
    goals: list[Goal]

class GoalsUpdate(BaseModel):
    name: Optional[str]
    frequency: Optional[str]
    icon_url: Optional[str]
    start_date: Optional[str]


class ActionParams(BaseModel):
    date_completed: date
    # goal_id: str

class Action(ActionParams):
    id: str
    user_id: str

class ActionsList(BaseModel):
    actions: list[Action]
