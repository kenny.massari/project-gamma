from fastapi.testclient import TestClient
from main import app
client = TestClient(app)
def test_get_token():
    auth_response = client.post(
        "/token", data={"username": "testuser", "password": "testpassword"}
    )
    access_token = auth_response.json().get("access_token")
    response = client.get(
        "/token", headers={"Authorization": f"Bearer {access_token}"}
    )
    data = response.json()
    assert response.status_code == 200
    assert isinstance(data, dict) or data is None
    if data:
        assert set(data.keys()) == {"account", "access_token", "type"}
        assert isinstance(data["access_token"], str)
        assert data["type"] == "Bearer"
