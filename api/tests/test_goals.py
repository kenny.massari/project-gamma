from fastapi.testclient import TestClient
from main import app
from queries.goals import GoalsQueries
from authenticator import authenticator
from models import Goal, GoalParams

client = TestClient(app)

def fake_get_current_account_data():
    return {
        'id': 'fakeuser'
    }

class FakeGoalsQueries:
    def get_by_id(self, id: str, user_id: str):
        return {
                "name": "one",
                "frequency": "10",
                "icon_url": "string",
                "start_date": "string",
                "id": id,
                "user_id": "64401bbd97bf15a927aba61e"
            }


def test_get_goal_by_id():
    # arrange
    app.dependency_overrides[GoalsQueries] = FakeGoalsQueries
    app.dependency_overrides[authenticator.get_current_account_data] = fake_get_current_account_data
    goal_id = "64401bdb97bf15a927aba61f"


    # act
    response = client.get(f'/api/goals/{goal_id}')

    # assert
    assert response.status_code == 200
    assert response.json() == { "name":'one',"frequency": "10",
        "icon_url": "string",
        "start_date": "string","id":goal_id, "user_id":"64401bbd97bf15a927aba61e",}

    # Cleanup
    app.dependency_overrides = {}


class MockGoalQueries:
    def create(self, params: GoalParams, user_id: str) -> Goal:
        goal = params.dict()
        goal['id'] = '64536'
        goal['name'] = 'run'
        goal['user_id'] = user_id
        return(Goal(**goal))

def test_create_goal():
    app.dependency_overrides[GoalsQueries] = MockGoalQueries
    app.dependency_overrides[authenticator.get_current_account_data] = fake_get_current_account_data
    goal = {
        'name': 'run',
        'frequency': '5',
        'icon_url': 'na',
        'start_date': '2022_04_22'
    }
    res = client.post('/api/goals', json=goal)
    data = res.json()
    print("data:", data)
    assert data['id'] == '64536'
    assert data['name'] == 'run'
    assert data['user_id'] == 'fakeuser'
    app.dependency_overrides = {}
