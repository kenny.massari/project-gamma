from models import ActionParams, Action
from queries.client import Queries
from bson.objectid import ObjectId
from fastapi.encoders import jsonable_encoder
from datetime import datetime, date


class ActionsQueries(Queries):
    COLLECTION = "actions"

    def create(self, params: ActionParams, user_id:str, goal_id: str, date_completed:str) -> Action:
        action = params.dict()
        print("params", params)
        action['user_id'] = user_id
        action['goal_id'] = goal_id
        date_completed_obj = datetime.fromisoformat(str(date_completed))
        action['date_completed'] = jsonable_encoder(date_completed_obj.date())
        self.collection.insert_one(action)
        action["id"] = str(action["_id"])
        return Action(**action)

    def get_all(self, goal_id: str, date_completed: date, user_id: str) -> list[Action]:
        actions = []
        for action in self.collection.find({"goal_id": goal_id, "date_completed": str(date_completed), "user_id": user_id}):
            action["id"] = str(action["_id"])
            actions.append(Action(**action))
        return actions

    def delete(self, id: str, user_id: str) -> bool:
        result = self.collection.delete_one({"_id": ObjectId(id), 'user_id': user_id})
        return result.deleted_count == 1

    def get_all_by_goal(self, goal_id: str, user_id: str) -> list[Action]:
        actions = []
        for action in self.collection.find({"goal_id": goal_id, "user_id": user_id}):
            action["id"] = str(action["_id"])
            actions.append(Action(**action))
        return actions
