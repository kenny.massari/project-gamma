from models import GoalParams, Goal, GoalsUpdate
from queries.client import Queries
from bson.objectid import ObjectId


class GoalsQueries(Queries):
    COLLECTION = "goals"

    def create(self, params: GoalParams, user_id: str) -> Goal:
        goal = params.dict()
        goal["user_id"] = user_id
        self.collection.insert_one(goal)
        goal["id"] = str(goal["_id"])
        return Goal(**goal)

    def get_all(self, user_id: str) -> list[Goal]:
        goals = []
        for goal in self.collection.find({"user_id": user_id}):
            goal["id"] = str(goal["_id"])
            goals.append(Goal(**goal))
        return goals

    def get_by_id(self, id: str, user_id: str) -> Goal:
        goal = self.collection.find_one({"_id": ObjectId(id), "user_id": user_id})
        if goal:
            goal["id"] = str(goal["_id"])
            return Goal(**goal)
        else:
            return None

    def delete(self, id: str, user_id: str) -> bool:
        result = self.collection.delete_one(
            {"_id": ObjectId(id), "user_id": user_id}
        )
        return result.deleted_count == 1

    def put(self, id: str, params: GoalParams, user_id: str) -> GoalsUpdate:
        print(params, "*********************************")
        result = self.collection.update_one(
            {"_id": ObjectId(id), "user_id": user_id},
            {"$set": params.dict(exclude_unset=True)},
        )
        if result.modified_count == 1:
            updated_goal = self.collection.find_one({"_id": ObjectId(id)})
            return GoalsUpdate(**updated_goal)
        else:
            return None
