from fastapi import APIRouter, Depends, Response
from queries.goals import GoalsQueries
from models import GoalParams, GoalsList, Goal, GoalsUpdate, GoalId
from authenticator import authenticator
from pydantic import BaseModel

router = APIRouter()

@router.post('/api/goals', response_model=Goal)
def create_goal(
    params: GoalParams,
    repo: GoalsQueries = Depends(),

    account_data: dict = Depends(authenticator.get_current_account_data)
):
    print(repo),
    return repo.create(params, user_id=account_data['id'])


@router.get('/api/goals', response_model=GoalsList)
def get_all_goals(
    repo: GoalsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return {
        'goals': repo.get_all(user_id=account_data['id'])
    }

@router.get('/api/goals/{id}', response_model=Goal)
def get_goal_by_id(
    id: str,
    repo: GoalsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    goal = repo.get_by_id(id=id, user_id=account_data['id'])
    return goal



@router.delete('/api/goals/{goal_id}', response_model=bool)
def delete_goal(
    goal_id: str,
    repo: GoalsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.delete(goal_id,user_id=account_data['id'])

class test(BaseModel):
    content: dict

@router.put('/api/goals/{goal_id}', response_model=GoalsUpdate)
def update_goal(
    goal_id: str,
    # response: Response,
    params: GoalsUpdate,
    repo: GoalsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    print(params, "these are the params in the router ************")
    return repo.put(goal_id, params,user_id=account_data['id'])
