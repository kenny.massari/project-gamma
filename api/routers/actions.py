from fastapi import APIRouter, Depends
from queries.actions import ActionsQueries
from models import ActionParams, Action, ActionsList
from authenticator import authenticator
from datetime import date, datetime
from fastapi.encoders import jsonable_encoder

router = APIRouter()


@router.post('/api/goals/{goal_id}/actions', response_model=Action)
def create_action(
    goal_id: str,
    params: ActionParams,
    repo: ActionsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.create(params, user_id=account_data['id'], goal_id=goal_id, date_completed=params.date_completed)


@router.get('/api/goals/{goal_id}/actions/{date_completed}', response_model=ActionsList)
def get_all_actions_by_date(
    goal_id: str,
    date_completed: date,
    repo: ActionsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return {
        'actions': repo.get_all(goal_id, date_completed, user_id=account_data['id'])
    }

@router.get('/api/goals/{goal_id}/actions/', response_model=ActionsList)
def get_all_actions_by_goal(
    goal_id: str,
    repo: ActionsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return {
        'actions': repo.get_all_by_goal(goal_id, user_id=account_data['id'])
    }




@router.delete('/api/actions/{action_id}', response_model=bool)
def delete_action(
    action_id: str,
    repo: ActionsQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    return repo.delete(action_id,user_id=account_data['id'])
