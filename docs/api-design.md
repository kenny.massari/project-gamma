### Signup

- Endpoint path: /api/signup
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Response: Signup success message
- Response shape:
  ```json
  {
    "signup": [
      {
        "email": str,
        "password": str,
        "confirm_password": str
      }
    ]
  }
  ```

### Log in

- Endpoint path: /api/token
- Endpoint method: POST

- Request shape (form):

  - username: string
  - password: string

- Response: Account information and a token
- Response shape (JSON):
  ```json
  {
    "account": {
      «key»: type»,
    },
    "token": str
  }
  ```

### Log out

- Endpoint path: /api/token
- Endpoint method: DELETE

- Headers:

  - Authorization: Bearer token

- Response: Always true
- Response shape (JSON):
  ```json
  true
  ```

### Create GOALS

POST /api/goals

Body

```json
{
    "name": str,
    "frequency": int,
    "icon_url": str,
    "start_date": date
}
```

### Delete goals

DELETE /api/goals/{goal_id}

Body

```json
{
    true
}
```

### Update goals

PUT /api/goals/{goal_id}

Body

```json
{
    "name": str,
    "frequency": int,
    "icon_url": str,
    "start_date": date
}
```

### GET GOALS LIST

GET /api/goals

```json
{
    "goals": [
        {
            "id": str,
            "user_id": str,
            "name": str,
            "frequency": int,
            "icon_url": str,
            "start_date": date
        }
    ]
}
```

### GET GOALS detail

GET /api/goals/{goal_id}

```json
{
    "id": str,
    "user_id": str,
    "name": str,
    "frequency": int,
    "icon_url": str,
    "start_date": date
}
```

### Compare length of list of actions to frequency set in goals

GET /api/goals/{goal_id}/actions?date=2023-03-27
Body

```json
{
    "actions": [
        {
            "user_id": str,
            "id": str,
            "goal_id": str,
            "date": str
        }
    ]
}
```

### Create action

POST /api/goals/{goal_id}/actions

Body

```json
{
    "user_id": str,
    "id": str,
    "goal_id": str,
    "date": str
}
```

### Delete action

DELETE /api/goals/{goal_id}/actions

Body

```json
{
    true
}
```
