# 3/27/2023

Morning - Worked together as a group to finish api creation outline.

# 3/29/2023

Worked together as a group to write create, get all, and delete
routers and queries for Actions. Ran into an issue filtering for date and
group id. We were not able to fully resolve the issue so only the delete route was working properly.

# 3/30/2023

Worked together as a group to resolve the filtering issues for creating and getting actions. All action routers and queries are returning 200s and working properly.

# 4/17/2023

Worked together as a group to get update goals function working. We were able to get the update form to show up but could not get the form to populate with the corresponding details of the goal selected. The get goal by id request is getting a 401 invalid token error. We were unable to resolve this issue. We also installed tailwind to start formatting and utilizing modals for some of our forms.

# 4/24/2023

Worked together as a group to fix update goals. Also, fixed rerouting for several form submissions -- updating a goal, signing in, logging out, and creating a new goal. Added the counter for each goal item to show count, frequency, and completed when count = frequency.

# 4/25/2023

Worked together to fix deleting action and decrementing the counter for goal items. Began working on unit testing.

# 4/26/2023

Worked together to getting unit tests passing. Worked on styling some forms.
